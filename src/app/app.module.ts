import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavComponent } from './components/nav/nav.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { FavoriteSongComponent } from './components/favorite-song/favorite-song.component';
import { RegisterComponent } from './components/register/register.component';
import { InsertSongComponent } from './components/insert-song/insert-song.component';

// Routes
import {app_routing} from './app.router'

//Services
import { HomeService } from './services/home.service';
import { UserService } from './services/user.service';

//necesarios
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HomeComponent,
    LoginComponent,
    FavoriteSongComponent,
    RegisterComponent,
    InsertSongComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    HomeService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
