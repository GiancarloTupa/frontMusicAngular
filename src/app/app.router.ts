import { RouterModule, Routes } from '@angular/router';

//components
import { HomeComponent } from './components/home/home.component';
import { FavoriteSongComponent } from './components/favorite-song/favorite-song.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { InsertSongComponent } from './components/insert-song/insert-song.component';

const APP_ROUTES: Routes = [
    {path:'home', component:HomeComponent},
    {path:'login', component:LoginComponent},
    {path:'favoriteSong', component:FavoriteSongComponent},
    {path:'register', component:RegisterComponent},
    {path:'insert-song', component:InsertSongComponent},
    {path:'**', pathMatch:'full', redirectTo: 'home'} 
];

export const app_routing = RouterModule.forRoot(APP_ROUTES);