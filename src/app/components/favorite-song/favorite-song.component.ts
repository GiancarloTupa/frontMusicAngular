import { Component, OnInit } from '@angular/core';
import { Songs, HomeService } from '../../services/home.service';


@Component({
  selector: 'app-favorite-song',
  templateUrl: './favorite-song.component.html',
  styleUrls: ['./favorite-song.component.css']
})
export class FavoriteSongComponent implements OnInit {

  songs:Songs[];


  constructor(public homeService:HomeService) { }

  ngOnInit() {
    this.songs = this.homeService.getSongsFav();
  }

  search(dato:string){
    console.log(dato);
    this.homeService.searchSong(dato);
  }

  deleteSongFav(id:string){
    let idsong = this.homeService.getsongIdSong(id) 
    //  console.log(this.homeService.getsongIdSong(id));

    // this.homeService.getSongFavforId(id);

    //  let idsong = this.homeService.getSongFavforId(id);
      // console.log(this.homeService.getSongFavforId(id));
    

    this.homeService.deleteSongfav(idsong, function(check){
      if(check){
        console.log("se elimino la canción favorita");
        
      }else{
        console.log("no se elimino la canción favorita")
      }
    });
    
  }



}
