import { Component, OnInit } from '@angular/core';
import { Songs, HomeService } from '../../services/home.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  songs:Songs[];
  constructor(public homeService:HomeService, private router:Router) { }

  ngOnInit() {
    this.songs=this.homeService.getSongs();
  }

  search(dato:string){
    console.log(dato);
    this.homeService.searchSong(dato);
  }

  insertSongFav(id:string){
    let idSong = this.homeService.getsongIdSong(id);
    console.log(idSong);
    this.homeService.addSongfav(idSong, function(check){
      if(check){
        console.log("se agrego canción favorita");
      }else{
        console.log("no se agreago la canción favorita")
      }
    });
    // this.router.navigate(['/login']);
  }

}
