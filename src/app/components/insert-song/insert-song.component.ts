import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-insert-song',
  templateUrl: './insert-song.component.html',
  styleUrls: ['./insert-song.component.css']
})
export class InsertSongComponent implements OnInit {

  fileUploadImage:File = null;
  fileUploadsong:File = null;

  constructor(private homeService:HomeService) { }

  ngOnInit() {
  }

  addSong(formSong){
    this.homeService.addSong(formSong.value, this.fileUploadImage, this.fileUploadsong);
  }

  handleFileInputImage(files:FileList){
    this.fileUploadImage = files.item(0);
  }

  handleFileInputSong(files:FileList){
    this.fileUploadsong = files.item(0);
  }

}
