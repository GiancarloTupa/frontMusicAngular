import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { HomeService } from '../../services/home.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  
  constructor(private userService:UserService, private homeService:HomeService) { }

  ngOnInit() {
  }

  logout(){
    this.userService.logout();
    // console.log(this.loginService);
    // this.songs=this.favoriteService.getSongsFav();
    // this.songs[""];
    // this.homeService.emptySongs();
  }
  

}
