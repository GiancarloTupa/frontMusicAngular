import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private user:UserService) { }

  ngOnInit() {
  }

  addUser(formUser){
    console.log(formUser.value);
    this.user.register(formUser.value)
  }

}
