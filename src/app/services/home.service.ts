import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';




export interface Songs{
  _id:string,
  name:string,
  artist:string,
  disk:string,
  gender:string,
  image:string,
  song:string
}


@Injectable()
export class HomeService {

  songsDisplay:Songs[]=[];
  songs:Songs[]=[];

  urlBase:string = "http://localhost:3977/api/";
  errors:string = "";

  constructor(private http:HttpClient, private user:UserService, private router:Router) { 
    console.log("servicio listo para usarse")
  }

  getSongs():any{
    this.songs = [];
    this.http.get(this.urlBase + 'songs').subscribe(data => {
      for(let i in data){
        this.songs.push(data[i])
      }
      this.songsDisplay = this.songs;
      console.log("descarga de canciones");
    }, err => {
      console.log(err);
    //  this.errors = err.message;    
    });
  }

  searchSong(dato:string){
    let song_list:Songs[]=[];
    for (let song of this.songs) {
      if(song.name.toLowerCase().includes(dato.toLowerCase())){
        song_list.push(song);
      }
    }
    this.songsDisplay = song_list;
  }

  uploadImage(fileToUploadImage: File, songId:string){
    // para subir una imagen, tal y como hemos hecho en postman necesitamos enviarle un formData con la imagen.
    const formData: FormData = new FormData();
    formData.append('image', fileToUploadImage, fileToUploadImage.name);
    // Concatenamos urlBase con el endpoint donde queremos apuntar.
    this.http
      .put(this.urlBase+"song/insert-image/"+songId, formData).subscribe(resp =>{
        console.log(resp);
      }, err =>{
        console.log(err);
      });
  }

  uploadSong(fileToUploadSong: File, songId:string){
    // para subir una imagen, tal y como hemos hecho en postman necesitamos enviarle un formData con la imagen.
    const formData: FormData = new FormData();
    formData.append('song', fileToUploadSong, fileToUploadSong.name);
    // Concatenamos urlBase con el endpoint donde queremos apuntar.
    this.http
      .put(this.urlBase+"song/insert-song/"+songId, formData).subscribe(resp =>{
        console.log(resp);
      }, err =>{
        console.log(err);
      });
  }

  addSong(song:Songs, fileToUploadImage:File, fileToUploadSong:File){
    // En este caso realizamos una llamada POST y le mandamos un body
    // Este metodo necesita validacion por token en la API, por lo tanto necesitamos enviarle un header con el token   
    // El token lo sacamos de localStorage ya guardado previamente.
    let headers = new HttpHeaders({"authorization":this.user.getToken()})

    this.http.post(this.urlBase+"song", song, {headers}).subscribe((resp:any) =>{
      // Imprimimos el objeto que hemos insertado.
      console.log(resp);
      // Si todo ha ido bien procedemos a insertar la imagen y para ello necesitamos el ID que nos ha devuelto la API
      this.uploadImage(fileToUploadImage, resp._id);
      this.uploadSong(fileToUploadSong, resp._id);
      this.router.navigate(['/home']);
    },
    (err)=>{
      console.log("error");
      console.log(err.message);
      // Si hay un error redirigimos al login.
      this.router.navigate(['/login']);
    });
  }

  getsongIdSong(id:string):string{
    for (let x of this.songs){
      if (x._id == id){
        return x._id;
      }
    }
  }




  addSongfav(idSong:string, cb){
    let headers = new HttpHeaders({"authorization":this.user.getToken()})

    this.http.post(this.urlBase+"songfav", {idSong}, {headers}).subscribe((resp:any) =>{
    
      console.log(resp);
      cb(true);
      this.router.navigate(['/favoriteSong']);
    },
    (err)=>{
      console.log("error");
      console.log(err.message);
      // Si hay un error redirigimos al login.
      cb(false);
      this.router.navigate(['/login']);
    });
  }

  
  deleteSongfav(id:string,cb){
    let headers = new HttpHeaders({"authorization":this.user.getToken()})

    this.http.delete(this.urlBase+"songfav/"+id, {headers}).subscribe((resp:any) =>{
    
      console.log(resp);
      cb(true);
      this.getSongsFav();
      this.router.navigate(['/favoriteSong']);
    },
    (err)=>{
      console.log("error");
      console.log(err.message);
      // Si hay un error redirigimos al login.
      cb(false);
      this.router.navigate(['/login']);
    });
  }

  getSongsFav():any{
    this.songs = [];
    let headers = new HttpHeaders({"authorization":this.user.getToken()});
    this.http.get(this.urlBase + 'songsfav', {headers}).subscribe(data => {
      for(let index in data){
        this.songs.push(data[index])
      }
      this.songsDisplay = this.songs;
      console.log("descarga de canciones");
      console.log(this.songsDisplay);
   }, err => {
    // console.log(err);
      //  this.errors = err.message
      this.router.navigate(['login']);
   });
  }
}
