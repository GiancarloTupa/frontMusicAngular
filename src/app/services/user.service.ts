import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

export interface User{
  name:string,
  surname:string,
  age:number,
  email:string,
  password:string
}

const TOKEN_NAME = "current_user";

@Injectable()
export class UserService {

  urlBase:string = "http://localhost:3977/api/";
 

  constructor(public http:HttpClient, private router:Router) { }

  // register(formUser){
   register(user:User)  {  

    this.http.post(this.urlBase+"user",user 
                  //  JSON.stringify(formUser)
     )
    
    .subscribe( resp =>{
      console.log(resp);
      this.router.navigate(['/login'])
    }, err =>{
      console.log(err);
    });
  }

  login(email:string, password:string, cb){
    this.http.post(this.urlBase+"login", {email, password}).subscribe((resp:any) => {
      console.log(resp);
      this.setToken(resp.token);
      console.log(resp.token);
      cb(true)
      this.router.navigate(['favoriteSong'])
    }, err => {
      console.log(err);
      cb(false)
    });
  }

  getToken(): string {
    return localStorage.getItem(TOKEN_NAME);
  }

  setToken(token: string): void {
    localStorage.setItem(TOKEN_NAME, token);
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    localStorage.removeItem(TOKEN_NAME);
  }
}
